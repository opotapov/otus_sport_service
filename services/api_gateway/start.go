package main

import (
	"net/http"
	"net/http/httputil"
	"net/url"
	"html/template"
	"log"
)

const OnlineEventsService = "http://otussportservice_online_events_service_1:80"

func main() {
	onlineEventsUrl, err := url.Parse(OnlineEventsService)
    if err != nil {
        log.Fatal(err)
    }

    http.HandleFunc("/matches/", matchHandler)
	http.Handle("/online-events/", httputil.NewSingleHostReverseProxy(onlineEventsUrl))
	http.HandleFunc("/", rootHandler)
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func rootHandler(w http.ResponseWriter, r *http.Request){
	t, _ := template.ParseFiles("index.html")
	t.Execute(w, nil)
}

func matchHandler(w http.ResponseWriter, r *http.Request){
	matchId := r.URL.Path[len("/matches/"):]
	t, _ := template.ParseFiles("match.html")
	t.Execute(w, matchId)
}