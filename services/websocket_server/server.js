var connectedClients = [];

var sendMessage = function(message){
    connectedClients.forEach(function(socket){
        socket.emit("message", message)
    })
}

var setupSocketIO = function () {
    var socket_app = require('express')();
    var socket_http = require('http').Server(socket_app);
    var io = require('socket.io')(socket_http, {
        cors: {
            origin: "http://localhost:8080",
            credentials: true,
            methods: ["GET", "POST"]
        }
    });

    io.on('connection', function (socket) {

        connectedClients.push(socket);

        socket.on('disconnect', function () {
            for (var i = 0; i < connectedClients.length; i++) {
                if (connectedClients[i] === socket) {
                    connectedClients.splice(i, 1);
                }
            }
            console.log('user disconnected');
        });
    });

    socket_http.listen(3000, function () {
        console.log('socket server listening on *:3000');
    });
};

var setupHTTP = function(){
    const express = require("express");
    var http_app = express();
    var http_server = require('http').Server(http_app);

    http_app.use(express.json());

    http_server.listen(80, function () {
        console.log('http server listening on *:80');
    });

    http_app.get('/health', function (req, res) {
        res.send('status: up');
    });

    http_app.post('/events.json', function(req, res){
        console.log(req.body.payload);

        var payload = req.body.payload;
        sendMessage(payload);

        res.send('');
    })
};

setupSocketIO();
setupHTTP();