package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"
	"bytes"
	"net/http"

	kafka "github.com/segmentio/kafka-go"
)

const KafkaUrl = "otussportservice_kafka_1:9092"
const KafkaTopic = "online-events"
const KafkaGroupID = "" //"live_events"
const WebsocketServerUrl = "http://otussportservice_websocket_server_1:80/events.json"

type MatchEvent struct {
	id int64
	occured_at time.Time
	match_id string
	tournament_id string
	event_type int64
	payload string
	source_id int64
}

func main() {
	reader := getKafkaReader()

	defer reader.Close()

	fmt.Println("start consuming ... !!")
	for {
		m, err := reader.ReadMessage(context.Background())
     	fmt.Println("mess read")
     	fmt.Println(m)

		if err != nil {
	     	fmt.Println("errror")
	     	fmt.Println(err)

			log.Fatalln(err)
		}
		fmt.Printf("message at topic:%v partition:%v offset:%v	%s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

		body := []byte(string(m.Value))


		resp, resp_err := http.Post(WebsocketServerUrl, "application/json", bytes.NewBuffer(body))

		defer resp.Body.Close()

		if resp_err != nil {
	     	fmt.Println(resp_err)
		}
	}
}


func getKafkaReader() *kafka.Reader {
	brokers := strings.Split(KafkaUrl, ",")
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  KafkaGroupID,
		Topic:    KafkaTopic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
}