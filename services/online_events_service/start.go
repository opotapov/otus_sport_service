package main

import (
	"net/http"
	"log"
	"fmt"
	"time"
	"strconv"
	"math/rand"
	"encoding/json"

	kafka "github.com/segmentio/kafka-go"
)

const KafkaUrl = "otussportservice_kafka_1"
const KafkaTopic = "online-events"

type MatchEvent struct {
	Id int64 `json:"id"`
	OccuredAt time.Time `json:"occured_at"`
	MatchId string `json:"match_id"`
	TournamentId string `json:"tournament_id"`
	EventType int64 `json:"event_type"`
	Payload string `json:"payload"`
	SourceId int64 `json:"source_id"`
}

func main() {
	http.HandleFunc("/online-events/health", healthHandler)
	http.HandleFunc("/online-events/events.json", postEventHandler)
	log.Fatal(http.ListenAndServe(":80", nil))
}

func healthHandler(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Status=OK")
}


func postEventHandler(w http.ResponseWriter, r *http.Request){
	r.ParseForm()
	log.Print("here1")

	id := generateID()
    event_type, _ := strconv.ParseInt(r.FormValue("event_type"), 10, 8)
    source_id, _ := strconv.ParseInt(r.FormValue("source_id"), 10, 16)
	event := MatchEvent{
		id,
		time.Now(),
		r.FormValue("match_id"),
		r.FormValue("tournament_id"),
		event_type,
		r.FormValue("payload"),
		source_id,
	}

	jsonEvent, _ := json.Marshal(event)

	kafkaWriter := getKafkaWriter()
	defer kafkaWriter.Close()

	msg := kafka.Message{
		Value: []byte(jsonEvent),
	}
	err := kafkaWriter.WriteMessages(r.Context(), msg)

	if err != nil {
		log.Fatalln(err)
	}
}

func generateID() int64 {
	return rand.Int63()
}

func getKafkaWriter() *kafka.Writer {
	return &kafka.Writer{
		Addr:     kafka.TCP(KafkaUrl),
		Topic:    KafkaTopic,
		Balancer: &kafka.LeastBytes{},
	}
}